// Main app logic

const
  // Initialize new camera instance on the player node
  camera = new Camera(document.getElementById('player')),


  _init = () => {
    const
      // Initialize new messages instance
      messages = new Message()

    // Notify user of connection errors
    window.addEventListener('messages_error', () => {
      toastr.error('Messages could not be retrieved.<br>Will keep trying.', 'Network Connection Error')
    })

    // Listen for existing messages event
    window.addEventListener('message_ready', e => {
      // Remove loader
      $('#loader').remove()

      // Check some messages exists
      if(1 > messages.all.length) {
        toastr.info('Add the first message.', 'No Messages')
      }

      // Empty out existing messages if this is from a reconnection
      $('#messages').empty()

      // Render each message
      messages.all.reverse().forEach(renderMessage)
    })

    // Listen for new message event
    window.addEventListener('message_received', e => {
      renderMessage(e.detail)
    })

    // Switch on camera in viewfinder
    $('#viewfinder').on('show.bs.modal', () => {
      camera.switch_on()
    })

    // Switch off camera in viewfinder
    $('#viewfinder').on('hidden.bs.modal', () => {
      camera.switch_off()
    })

    // Take photo
    $('#shutter').on('click', () => {
      const
        photo = camera.take_photo()

      // Show photo preview in camera button
      $('#camera')
        .css('background-image', `url(${photo})`)
        .addClass('withphoto')
    })

    // Submit messahe
    $('#send').on('click', () => {
      const
        // Get caption text
        caption = $('#caption').val()

      if(!camera.photo || !caption) {
        toastr.warning('Photo & Caption Required.', 'Incomplete Message')

        return
      }

      const
        msg = messages.add(camera.photo, caption)

      renderMessage(msg)

      // Reset caption field on success
      $('#caption').val('')

      $('#camera')
        .css('background-image', '')
        .removeClass('withphoto')

      camera.photo = null
    })
  },

  // Create new message element
  renderMessage = ({photo, caption}) => {
    const
      html = `
      <div class="row message bg-light mb-2 rounded shadow" style="display: none;">
        <div class="col-2 p-1">
          <img src="${photo}" class="photo w-100 rounded" />
        </div>
        <div class="col-10 p-1">${caption}</div>
      </div>
      `

    // Prepend to #messages
    $(html)
      .prependTo('#messages')
      .show(500)

      // Bind click handler on new image to show a modal
      .find('img').on('click', showPhoto)
  },

  // Show message photo in modal
  showPhoto = e => {
    const
      // Get photo src
      photoSrc = $(e.currentTarget).attr('src')

    // Set photo/image source & show modal
    $('#photoframe img').attr('src', photoSrc)

    $('#photoframe').modal('show')
  }

// Initialize app if supported
if(navigator.mediaDevices) {
  // Init app
  _init()

  // Register SW if supported
  if(navigator.serviceWorker) {
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/sw.js')
    })
  }
}
else {
  // Show notification
  toastr.error(
    null,
    'This app is not supported by your browser.',
    { 'timeout': 30000 }
  )
}

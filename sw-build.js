
const
  // Require WorkBox build
  { generateSW } = require('workbox-build')

generateSW({
  'swDest': 'src/client/sw.js',

  'globDirectory': 'src/client',
  'globPatterns': [
    '**/*.{html,css}',
    'main.js',
    'Classes/*.js'
  ],

  'skipWaiting': true,
  'clientsClaim': true,

  'runtimeCaching': [
    {
      'urlPattern': /\.(css|js)/,
      'handler': 'cacheFirst'
    },
    {
      'urlPattern': /^https:\/\/use\.fontawesome\.com.*/,
      'handler': 'staleWhileRevalidate',
      'options': {'cacheName': 'fontawesome'}
    }
  ]
})
  .then(({count, size}) => console.log(`Generated new service worker with ${count} precached files, totaling ${size} bytes.`))
  .catch(console.error)
